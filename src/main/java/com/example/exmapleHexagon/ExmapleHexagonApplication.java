package com.example.exmapleHexagon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExmapleHexagonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExmapleHexagonApplication.class, args);
	}

}
