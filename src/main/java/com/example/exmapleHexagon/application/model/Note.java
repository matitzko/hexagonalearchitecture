package com.example.exmapleHexagon.application.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Note extends XMLEntity{


    @SequenceGenerator(
            name="note_id",
            sequenceName ="note_id",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "note_id"
    )
    @Id
    private Long id;
    private String receiver;
    private String sender;
    private String heading;
    private String message;
}
