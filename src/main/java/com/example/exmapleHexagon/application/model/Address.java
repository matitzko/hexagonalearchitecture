package com.example.exmapleHexagon.application.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Setter
@Getter
public class Address extends XMLEntity{


    @SequenceGenerator(
            name="addresse_sequence",
            sequenceName = "addresse_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "addresse_sequence"
    )
    @Id
    Long id;
    String name;
    String street;
    String phoneNumber;
}
