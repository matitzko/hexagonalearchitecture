package com.example.exmapleHexagon.application;

import com.example.exmapleHexagon.application.mapper.MapperRegistry;
import com.example.exmapleHexagon.application.mapper.MappingCapable;
import com.example.exmapleHexagon.application.mapper.MessageType;
import com.example.exmapleHexagon.application.model.XMLEntity;
import com.example.exmapleHexagon.port.in.XMLFileImporterPort;
import com.example.exmapleHexagon.port.out.XMLMessageSaverPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class XMLImportService implements XMLFileImporterPort {

    @Autowired
    MapperRegistry mapperRegistry;
    @Autowired
    XMLMessageSaverPort xmlMessageSaverPort;

    public XMLImportService(MapperRegistry mapperRegistry, XMLMessageSaverPort xmlMessageSaverPort){
        this.mapperRegistry = mapperRegistry;
        this.xmlMessageSaverPort =  xmlMessageSaverPort;
    }


    @Override
    public void importXMLFile(String file) throws FileNotFoundException, XMLStreamException {
        File xmlFile = new File(file);
        MessageType xmlType = this.getXmlType(xmlFile);
        List<XMLEntity> xmls  = new ArrayList<>();
        Optional<MappingCapable> messageMapper = mapperRegistry.getMessageMapper(xmlType);
        if (messageMapper.isPresent()) {
             xmls  =messageMapper.get().mapToModels(xmlFile);
        }



        xmls.stream().forEach(xml -> xmlMessageSaverPort.saveXML(xml));

    }


    public MessageType getXmlType(File xmlFile) throws FileNotFoundException, XMLStreamException {
        InputStream inputStream = new FileInputStream(xmlFile);
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(inputStream);

        while (xmlEventReader.hasNext()) {
            XMLEvent event = xmlEventReader.nextEvent();

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String elementName = startElement.getName().getLocalPart();
                if (elementName.equals("MESSAGE_TYPE")) {
                    event = xmlEventReader.nextEvent();
                    String messageType = event.asCharacters().getData();
                    return MessageType.valueOf(messageType.toUpperCase());
                }

            }
        }
        return null;
    }
}
