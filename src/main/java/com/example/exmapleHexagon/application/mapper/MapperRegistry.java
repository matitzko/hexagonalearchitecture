package com.example.exmapleHexagon.application.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.Optional;

@Service
public class MapperRegistry {

    private EnumMap<MessageType, MappingCapable> mappers =  new EnumMap<MessageType, MappingCapable>(MessageType.class);

    @Autowired
    public MapperRegistry(NoteMapper noteMapper, AddressMapper addressMapper){
        mappers.put(MessageType.NOTE, noteMapper);
        mappers.put(MessageType.ADDRESS, addressMapper);
    }

    public Optional<MappingCapable> getMessageMapper(MessageType type) {
        return Optional.ofNullable(mappers.get(type));
    }


}
