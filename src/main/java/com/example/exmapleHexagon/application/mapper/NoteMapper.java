package com.example.exmapleHexagon.application.mapper;

import com.example.exmapleHexagon.application.model.Note;
import com.example.exmapleHexagon.application.model.XMLEntity;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class NoteMapper implements MappingCapable{

    static final String NOTE = "note";
    static final String ID = "id";
    static final String RECEIVER = "receiver";
    static final String SENDER = "sender";
    static final String HEADING = "heading";
    static final String MESSAGE = "message";

    @Override
    public List<XMLEntity> mapToModels(File xmlFile) throws FileNotFoundException, XMLStreamException {

        List<XMLEntity> notes = new ArrayList<>();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream inputStream = new FileInputStream(xmlFile);
        XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(inputStream);

        Note note = null;
        while (xmlEventReader.hasNext()) {
            XMLEvent event = xmlEventReader.nextEvent();

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String elementName = startElement.getName().getLocalPart();

                switch (elementName) {
                    case NOTE:
                        note = new Note();
                        break;
                    case ID:
                        event = xmlEventReader.nextEvent();
                        note.setId(Long.valueOf(event.asCharacters().getData()));
                        break;
                    case RECEIVER:
                        event = xmlEventReader.nextEvent();
                        note.setReceiver(event.asCharacters().getData());
                        break;
                    case SENDER:
                        event = xmlEventReader.nextEvent();
                        note.setSender(event.asCharacters().getData());
                        break;
                    case HEADING:
                        event = xmlEventReader.nextEvent();
                        note.setHeading(event.asCharacters().getData());
                        break;
                    case MESSAGE:
                        event = xmlEventReader.nextEvent();
                        note.setMessage(event.asCharacters().getData());
                        break;

                }
            }
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                if (endElement.getName().getLocalPart().equals(NOTE)) {
                    notes.add(note);
                }
            }
        }
        return notes;
    }

}
