package com.example.exmapleHexagon.application.mapper;

import com.example.exmapleHexagon.application.model.XMLEntity;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface MappingCapable {

    List<XMLEntity> mapToModels(File xmlFile) throws FileNotFoundException, XMLStreamException;
}
