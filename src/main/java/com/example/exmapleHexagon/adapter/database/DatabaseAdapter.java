package com.example.exmapleHexagon.adapter.database;

import com.example.exmapleHexagon.adapter.database.repository.AddressRepository;
import com.example.exmapleHexagon.adapter.database.repository.NoteRepository;
import com.example.exmapleHexagon.application.model.Address;
import com.example.exmapleHexagon.application.model.Note;
import com.example.exmapleHexagon.application.model.XMLEntity;
import com.example.exmapleHexagon.port.out.XMLMessageSaverPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DatabaseAdapter implements XMLMessageSaverPort {

    @Autowired
    AddressRepository addressRepository;
    @Autowired
    NoteRepository noteRepository;



    @Override
    public void saveXML(XMLEntity xmlEntity) {
        if(xmlEntity instanceof Address){
            Address address = (Address) xmlEntity;
            addressRepository.save(address);
        }
        if(xmlEntity instanceof Note){
            Note note = (Note) xmlEntity;
            noteRepository.save(note);
        }
    }
}
