package com.example.exmapleHexagon.adapter.database.repository;

import com.example.exmapleHexagon.application.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note,Long> {
}
