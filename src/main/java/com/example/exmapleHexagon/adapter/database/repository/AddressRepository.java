package com.example.exmapleHexagon.adapter.database.repository;

import com.example.exmapleHexagon.application.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
