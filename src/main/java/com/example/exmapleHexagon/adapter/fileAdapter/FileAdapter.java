package com.example.exmapleHexagon.adapter.fileAdapter;

import com.example.exmapleHexagon.port.in.XMLFileImporterPort;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.*;

@Service
public class FileAdapter {

    private XMLFileImporterPort xmlFileImporterPort;

    private static final String DIRECTORY="C:/Users/matthias.willers/Desktop/projectXmlImportDirectory";


    public FileAdapter(XMLFileImporterPort xmlFileImporterPort){
        this.xmlFileImporterPort = xmlFileImporterPort;
    }

    public void pollFiles() throws IOException, InterruptedException, XMLStreamException {

        Files.walk(Paths.get(DIRECTORY)).filter(Files::isRegularFile).forEach(file -> {
            try {
                xmlFileImporterPort.importXMLFile(file.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        });



        WatchService watchService  = FileSystems.getDefault().newWatchService();
        Path path = Paths.get(DIRECTORY);
        path.register(
                watchService,
                StandardWatchEventKinds.ENTRY_CREATE);

        WatchKey key;
        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {

                if(event.kind().equals(StandardWatchEventKinds.ENTRY_CREATE)) {
                    xmlFileImporterPort.importXMLFile(DIRECTORY + "/"+event.context());
                }
            }
            key.reset();


        }

    }
}
