package com.example.exmapleHexagon.port.out;


import com.example.exmapleHexagon.application.model.XMLEntity;

public interface XMLMessageSaverPort {

    void saveXML(XMLEntity xmlEntity);

}
