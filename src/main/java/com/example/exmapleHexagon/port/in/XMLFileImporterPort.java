package com.example.exmapleHexagon.port.in;

import com.example.exmapleHexagon.application.mapper.MappingCapable;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;

public interface XMLFileImporterPort {

    void importXMLFile(String file) throws FileNotFoundException, XMLStreamException;

}
