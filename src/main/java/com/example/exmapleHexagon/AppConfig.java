package com.example.exmapleHexagon;
import com.example.exmapleHexagon.adapter.fileAdapter.FileAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Autowired
    FileAdapter fileAdapter;


    @Bean
    CommandLineRunner commandLineRunner() {
        return args -> {
            fileAdapter.pollFiles();
        };
    }
}
